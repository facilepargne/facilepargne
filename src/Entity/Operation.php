<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OperationRepository")
 */
class Operation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(
     *      min=5, 
     *      max=255, 
     *      minMessage="L'intitulé de votre opération doit contenir au moins 5 caractères",
     *      maxMessage="L'intitulé de votre opération ne doit pas contenir plus de 255 caractères")
     */
    private $name; 

    /**
     * @ORM\Column(type="datetime")
     */
    private $createAt;

    /**
     * @ORM\Column(type="float")
     * @Assert\NotBlank(message="Le montant de votre opération doit être rempli")
     */
    private $amount;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="operations")
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\AccountingMonth", inversedBy="Operations")
     */
    private $accountingMonth;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(
     *      min=5, 
     *      max=255, 
     *      minMessage="Le nom du membre effectuant votre opération doit contenir au moins 5 caractères",
     *      maxMessage="Le nom du membre effectuant votre opération ne doit pas contenir plus de 255 caractères")
     */
    private $familyMember;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Assert\Length(
     *      min=5, 
     *      max=500, 
     *      minMessage="La description de votre opération doit contenir au moins 5 caractères",
     *      maxMessage="La description de votre opération ne doit pas contenir plus de 255 caractères")
     */
    private $description;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->createAt;
    }

    public function setCreateAt(\DateTimeInterface $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getCategory(): ?category
    {
        return $this->category;
    }

    public function setCategory(?category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getAccountingMonth(): ?AccountingMonth
    {
        return $this->accountingMonth;
    }

    public function setAccountingMonth(?AccountingMonth $accountingMonth): self
    {
        $this->accountingMonth = $accountingMonth;

        return $this;
    }

    public function getFamilyMember(): ?string
    {
        return $this->familyMember;
    }

    public function setFamilyMember(string $familyMember): self
    {
        $this->familyMember = $familyMember;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }
}
