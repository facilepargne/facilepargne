<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SavingController extends AbstractController
{
    /**
     * @Route("/saving", name="saving")
     * @Security("is_granted('ROLE_USER')", message="Vous devez vous connecter pour accéder à cette page !")
     */
    public function index()
    {
        return $this->render('saving/index.html.twig', [
            'controller_name' => 'SavingController',
        ]);
    }
}
