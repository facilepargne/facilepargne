<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TaxController extends AbstractController
{
    /**
     * @Route("/tax", name="tax")
     * @Security("is_granted('ROLE_USER')", message="Vous devez vous connecter pour accéder à cette page !")
     */
    public function index()
    {
        return $this->render('tax/index.html.twig', [
            'controller_name' => 'TaxController',
        ]);
    }
}
