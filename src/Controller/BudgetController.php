<?php

/**
 * Classe BudgetController
 */
namespace App\Controller;

use App\Entity\Category;
use App\Entity\Operation;
use App\Form\OperationType;

use App\Entity\AccountingMonth;
use App\Repository\AccountingMonthRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BudgetController extends AbstractController
{
    /**
     * @Route("/budget", name="budget")
     * @Security("is_granted('ROLE_USER')", message="Vous n'avez pas accès à cette ressource !")
     */
    public function index()
    {
        return $this->render('budget/index.html.twig', [
            'user' => $this->getUser()
        ]);
    }

    /**
     * Retourne la page affichant les opérations du mois donné en paramètre
     * 
     * @Route("/budget/{id}", name="operations_show")
     * @Security("is_granted('ROLE_USER') and user === accountingMonth.getAuthor()", message="Vous n'avez pas accès à cette ressource !")
     *
     * @return Response
     */
    public function showOperations(AccountingMonth $accountingMonth) 
    {
        return $this->render('budget/show-monthoperations.html.twig', [
            'user' => $this->getUser(),
            'accountingMonth' => $accountingMonth
        ]);
    }

    /**
     * @Route("/budget/operations/new", name="operation_create")
     * @Route("/budget/operations/{id}/edit", name="operation_edit")
     * @IsGranted("ROLE_USER")
     */
    public function formOperation(Operation $operation = null, Request $request, AccountingMonthRepository $repo)
    {
        // On récupère les comptes du mois
        $accountingMonth = $repo->findOneByActiveMonth(1);

        // On récupère un manager pour persister les données
        $manager = $this->getDoctrine()->getManager();

        if (!$operation) 
        {
            // Si l'opération n'existe pas, on souhaite quand même en créer une pour le formulaire
            $operation = new Operation();
        }

        $form = $this->createForm(OperationType::class, $operation);
        $form->handleRequest($request);

        // Si le formulaire a été envoyé et est valide
        if ($form->isSubmitted() && $form->isValid())
        {
            // Si notre opération n'existe pas déjà alors on affecte le mois comptable
            if (!$operation->getId()) {
                $operation->setAccountingMonth($accountingMonth);
            }

            $manager->persist($operation);
            $manager->flush();

            $this->addFlash(
                'success',
                "Votre opération s'est bien enregistrée !"
            );

            return $this->redirectToRoute('operation_show', ['id' => $operation->getId()]); 
        }

        return $this->render('budget/createoperation.html.twig', [
            'formOperation' => $form->createView(),
            'editMode' => $operation->getId() !== null
        ]);
    }

    /**
     * @Route("/budget/operations/{id}", name="operation_show")
     * 
     * @Security("is_granted('ROLE_USER') and user === operation.getAccountingMonth().getAuthor()", message="Vous n'avez pas le droit de voir cette opération si vous n'êtes pas son auteur")
     */
    public function showOperation(Operation $operation) 
    {
        return $this->render('budget/showoperation.html.twig', [
            'operation' => $operation
        ]);
    }
}
