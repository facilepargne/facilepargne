<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PreciousMetalController extends AbstractController
{
    /**
     * @Route("/preciousmetal", name="precious_metal")
     * @Security("is_granted('ROLE_USER')", message="Vous devez vous connecter pour accéder à cette page !")
     */
    public function index()
    {
        return $this->render('precious_metal/index.html.twig', [
            'controller_name' => 'PreciousMetalController',
        ]);
    }
}
