<?php

namespace App\Controller;

use App\Entity\Currency;
use App\Repository\CurrencyRepository;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TradingController extends AbstractController
{
    /**
     * @Route("/trading", name="trading")
     * @Security("is_granted('ROLE_USER')", message="Vous devez vous connecter pour accéder à cette page !")
     */
    public function index()
    {
        return $this->render('trading/index.html.twig', [
            'controller_name' => 'TradingController',
        ]);
    }

    /**
     * @Route("/trading/transactions", name="trading_transactions")
     */
    public function transactions()
    {
        return $this->render('trading/transactions.html.twig');
    }

    /**
     * @Route("/trading/transaction/12", name="transaction_show")
     */
    public function showTransaction()
    {
        return $this->render('trading/showtransaction.html.twig');
    }

    /**
     * @Route("/trading/currencies", name="currencies")
     */
    public function currencies(CurrencyRepository $repo) 
    {
        $currencies = $repo->findAll();

        return $this->render('trading/currencies.html.twig', [
            'currencies' => $currencies
        ]);
    }

    /**
     * @Route("/trading/currencies/{id}", name="currency_show")
     */
    public function showCurrency(Currency $currency) 
    {
        return $this->render('trading/showcurrency.html.twig', [
            'currency' => $currency
        ]);
    }
}
