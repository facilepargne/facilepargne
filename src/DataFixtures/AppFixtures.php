<?php

namespace App\DataFixtures;

use DateTime;
use Faker\Factory;
use App\Entity\Role;
use Faker\Generator;
use App\Entity\FeUser;
use App\Entity\Category;
use App\Entity\Currency;
use App\Entity\Operation;
use App\Entity\AccountingMonth;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    /**
     * 
     */
    private $categories = [];
    private $users = [];

    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr-FR');

        // On charge les devises
        $this->loadCurrencies($manager);
        // On charge les catégories
        $this->loadCategories($manager, $faker);
        // On charge les utilisateurs
        $this->loadUsers($manager, $faker);
        // On charge les mois comptables
        $this->loadAccountingMonth($manager, $faker);

        $manager->flush();
    }

    private function loadCurrencies(ObjectManager $manager)
    {
        $currencyEuro = new Currency();
        $currencyEuro->setName("Euro")
                     ->setShortName("EUR")
                     ->setAbbreviation("€");
        $manager->persist($currencyEuro);

        $currencyUsDollar = new Currency();
        $currencyUsDollar->setName("US Dollar")
                     ->setShortName("USD")
                     ->setAbbreviation("$");
        $manager->persist($currencyUsDollar);

        $currencyPound = new Currency();
        $currencyPound->setName("Pound")
                     ->setShortName("GBP")
                     ->setAbbreviation("£");
        $manager->persist($currencyPound);
    }

    private function loadCategories(ObjectManager $manager, Generator $faker) 
    {
        for ($i=0; $i<10; $i++)
        {
            $category = new Category();
            $category->setName($faker->word());
            $this->categories[$i] = $category;
            $manager->persist($this->categories[$i]);
        }
    }

    private function loadUsers(ObjectManager $manager, Generator $faker) 
    {
        $genders = ['male', 'female'];
        $adminRole = new Role();
        $adminRole->setTitle('ROLE_ADMIN');
        $manager->persist($adminRole);

        $adminUser = new FeUser();
        $adminUser->setFirstName('Chevrier')
                  ->setLastName('Jérémy')
                  ->setEmail('jeremy.chevrier@facilepargne')
                  ->setPassword($this->encoder->encodePassword($adminUser, 'rabbit'))
                  ->setPicture('https://randomuser.me/api/portraits/men/42.jpg')
                  ->setIntroduction($faker->sentence())
                  ->setDescription('<p>'.join('</p><p>', $faker->paragraphs(3)).'</p>')
                  ->addUserRole($adminRole);
        $manager->persist($adminUser);

        for ($i=0; $i < 10; $i++) { 
            $user = new FeUser();

            // On récupère une image d'utilisateur au hasard :)
            $gender = $faker->randomElement($genders);
            $picture = 'https://randomuser.me/api/portraits/';
            $pictureId = $faker->numberBetween(0, 99).'.jpg';
            $picture .= ($gender == 'male' ? 'men/' : 'women/').$pictureId;

            // On encode les mots de passes :)
            $password = $this->encoder->encodePassword($user, 'rabbit');

            $user->setFirstName($faker->firstname($gender))
                 ->setLastName($faker->lastname)
                 ->setEmail($faker->email)
                 ->setPicture($picture)
                 ->setIntroduction($faker->sentence())
                 ->setDescription('<p>'.join('</p><p>', $faker->paragraphs(3)).'</p>')
                 ->setPassword($password);
            $manager->persist($user);
                  
            $this->users[$i] = $user;
        }
    }

    private function loadAccountingMonth(ObjectManager $manager, Generator $faker)
    {
        for ($i=2010; $i < 2020; $i++) { 
            for ($j=1; $j < 13; $j++) { 
                // On créé le mois comptable
                $accountingMonth = new AccountingMonth();
                // On récupère un utilisateur au hasard
                $user = $this->users[mt_rand(0, count($this->users) -1)];

                $accountingMonth->setActiveMonth($j)
                                ->setActiveYear($i)
                                ->setCreateAt(new \DateTime($faker->dateTimeBetween($j.'\/01\/'.$i, $j.'\/05\/'.$i)->format('m/d/Y')))
                                ->setAuthor($user);
                $manager->persist($accountingMonth);

                $this->loadOperations($manager, $faker, $accountingMonth, $j, $i);
            }
        }
    }

    private function loadOperations(ObjectManager $manager, Generator $faker, $accountingMonth, $month, $year)
    {
        for ($i=0; $i < 35; $i++) { 
            $operation = new Operation();
            $operation->setName($faker->sentence())
                      ->setCreateAt(new \DateTime($faker->dateTimeBetween($month.'\/01\/'.$year, $month.'\/28\/'.$year)->format('m/d/Y')))
                      ->setAmount($faker->randomFloat(2, 0, 200))
                      ->setCategory($this->categories[mt_rand(0,9)])
                      ->setFamilyMember($faker->firstname)
                      ->setDescription($faker->text());
            $manager->persist($operation);

            $accountingMonth->addOperation($operation);
            $manager->persist($accountingMonth);
        }
    }
}
