<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\Operation;

use App\Form\ApplicationType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class OperationType extends ApplicationType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, $this->getConfiguration("Libellé", "Libellé de l'opétarion"))
            ->add('createAt', DateType::class, $this->getConfiguration("Date de l'opération", ""))
            ->add('amount', MoneyType::class, $this->getConfiguration("Montant", "Montant de l'opération, débit/crédit"))
            ->add('familyMember', TextType::class, $this->getConfiguration("Membre", "Nom du membre ayant effectué l'opération"))
            ->add('category', EntityType::class, [
                'class' => Category::class,
                'choice_label' => 'name'
            ])
            ->add('description', TextareaType::class, $this->getConfiguration("Description", "Description de l'opération"))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Operation::class,
        ]);
    }
}
